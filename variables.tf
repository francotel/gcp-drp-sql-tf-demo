variable "project_id" {
  type        = string
  description = "The name of the GCP Project"
  default     = "rich-synapse-400718"
}

variable "region_pri" {
  type        = string
  description = "The GCP Region Primary for the GCP Project"
  default     = "us-east1"
}

variable "region_sec" {
  type        = string
  description = "The GCP Region Secondary for the GCP Project"
  default     = "us-east1"
}