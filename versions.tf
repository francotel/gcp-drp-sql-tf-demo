###############################################################################
# Terraform specific configuration
###############################################################################
terraform {
  required_version = ">=0.12.29, <=1.7.2"
  required_providers {
    google = "~> 5.1.0"
  }
  backend "gcs" {
    bucket = "tf-state-orion-demo"
    prefix = "drp-sql-demo/state"
  }
}